import 'package:flutter/material.dart';
import 'package:tela_inicial/Pages/Login.dart';




void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Dog Life',
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: LoginPage()
    );
  }
}

